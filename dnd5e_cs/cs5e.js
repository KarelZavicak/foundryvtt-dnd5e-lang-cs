// DND5E-CS
// @author KarelZavicak
// @version 1.0.0
import { DND5E } from "../../systems/dnd5e/module/config.js";
import Actor from "../../systems/dnd5e/module/actor/sheets/character.js";
import NPC from "../../systems/dnd5e/module/actor/sheets/npc.js";

//Translate non localized strings from the DND5E.CONFIG
Hooks.once('ready', function () {
	let lang = game.i18n.lang;
	if (lang === "cs") {
		DND5E.armorProficiencies = {
			"lgt": "Lehké brnění",
			"med": "Střední brnění",
			"hvy": "Těžké brnění",
			"shl": "Štíty"
		};
		DND5E.abilityActivationTypes = {
			"none": "Žádná",
			"action": "Akce",
			"bonus": "Bonusová Akce",
			"reaction": "Reakce",
			"minute": "Minuta",
			"hour": "Hodina",
			"day": "Den",
			"special": "Speciální",
			"legendary": "Legendární Akce",
			"lair": "Akce doupěte"
		};
	}

});

Hooks.once('init', () => {

	if (typeof Babele !== 'undefined') {

		Babele.get().register({
			module: 'dnd5e_cs',
			lang: 'cs',
			dir: 'compendium'
		});

		Babele.get().registerConverters({
			"weight": (value) => {
				return parseInt(value) / 2
			},
			"range": (range) => {
				if (range) {
					if (range.units === 'ft') {
						if (range.long) {
							range = mergeObject(range, { long: range.long * 0.3 });
						}
						if (game.modules.get("metric-system-dnd5e").active) {
							return mergeObject(range, { value: range.value * 0.3, units: 'm' });
						}
						return mergeObject(range, { value: range.value * 0.3 });
					}
					if (range.units === 'mi') {
						if (range.long) {
							range = mergeObject(range, { long: range.long * 1.5 });
						}
						if (game.modules.get("metric-system-dnd5e").active) {
							return mergeObject(range, { value: range.value * 1.5, units: 'km' });
						}
						return mergeObject(range, { value: range.value * 1.5 });
					}
					return range;
				}
			},
			"target": (target) => {
				if (target) {
					if (target.units === 'ft') {
						if (game.modules.get("metric-system-dnd5e").active) {
							return mergeObject(target, { value: target.value * 0.3, units: "m" });
						}
						return mergeObject(target, { value: target.value * 0.3 });
					}
					return target;
				}
			}

		});
	}
});

export class ActorSheet5eCharacter extends Actor {
	static get defaultOptions() {
		return mergeObject(super.defaultOptions, {
			classes: ["cs5e", "dnd5e", "sheet", "actor", "character"],
			width: 800
		});
	}
}

export class ActorSheet5eNPC extends NPC {
	static get defaultOptions() {
		return mergeObject(super.defaultOptions, {
			classes: ["cs5e", "dnd5e", "sheet", "actor", "npc"],
			width: 700
		});
	}
}

Hooks.once('ready', function () {
	let lang = game.i18n.lang;
	if (lang === "cs") {
		Actors.registerSheet('dnd5e', ActorSheet5eCharacter, {
			types: ['character'],
			makeDefault: true
		});

		Actors.registerSheet('dnd5e', ActorSheet5eNPC, {
			types: ['npc'],
			makeDefault: true
		});
	}
});
